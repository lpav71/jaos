ANY - это любой указатель (см. [Сообщение о языке Active Oberon](https://gitlab.com/budden/jaos/tree/master/LanguageReport)), 
фраза «All pointer variables inherit the extension relation of the basetype ANY and are initialized to NIL.»

Как с ним работать? Можно использовать WITH, IS, Modules.TypeOf, Debugging.Type, а также безопасно преобразовывать его с помощью `значениеТипаANY(ЦелевойТипУказатель)`

Однако, если в такой переменной находится указатель на массив, то полная информация о его типе во время выполнения недоступна. 

Записать в ANY статический объект можно с помощью хакерских приёмов неизвестной надёжности, см. [Операция взятия адреса](операция-взятия-адреса.md)