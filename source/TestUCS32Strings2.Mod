MODULE TestUCS32Strings2; (** AUTHOR "staubesv"; PURPOSE "Testbed for Strings.Mod";
  Адаптировано для StringsUCS32. Это - ЧУДОВИЩНО долгие тесты. См
  TestUCS32Strings2 *)

IMPORT
	Commands, StringsUCS32, UCS32;

PROCEDURE TestLowerCase*(c : Commands.Context);
VAR s1,s2 : UCS32.PStringJQ;
BEGIN
s1 := StringsUCS32.NewString(UCS32.Lit("akzAKZаёляАЁЛЯ'^&")^);
s2 := StringsUCS32.NewString(UCS32.Lit("akzakzаёляаёля'^&")^);
StringsUCS32.LowerCase(s1^);
ASSERT(s1^ = s2^);
END TestLowerCase;

PROCEDURE TestLess*(c : Commands.Context);
VAR s1,s2 : UCS32.PStringJQ;
BEGIN
s1 := StringsUCS32.NewString(UCS32.Lit("б'^&")^);
s2 := StringsUCS32.NewString(UCS32.Lit("в'^&")^);
ASSERT(s1^ < s2^);
END TestLess;

PROCEDURE TestCharPrisv*(c : Commands.Context);
VAR c1,c2 : UCS32.CharJQ;
BEGIN
c1 := UCS32.CHARvCharJQ("k");
ASSERT(c1.UCS32CharCode = ORD("k"));
c2 := c1;
ASSERT(c2.UCS32CharCode = c1.UCS32CharCode);
END TestCharPrisv;


END TestUCS32Strings2.

System.DoCommands
	System.FreeDownTo TestUCS32Strings2~
	TestUCS32Strings2.TestLowerCase ~
	TestUCS32Strings2.TestLess ~
	TestUCS32Strings2.TestCharPrisv ~
~
	

System.Free  ~
