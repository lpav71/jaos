MODULE BtDTraps;
IMPORT SYSTEM, Kernel32, Objects, Modules, KernelLog, PodrobnajaPechatq;
CONST
	Trace = FALSE;
TYPE
	BrkPnt* = POINTER TO RECORD
		c*:CHAR; (*save char here*)
		adr*:SIGNED32;
		pos*:SIGNED32;
		active*:BOOLEAN;
		next*:BrkPnt;
	END;
	DebugHandler* = PROCEDURE {DELEGATE} ( VAR int: Kernel32.Context;  VAR exc: Kernel32.ExceptionRecord;  VAR handled: BOOLEAN );

VAR
	brkPnt*:BPList;
	exceptionhandler: Objects.ExceptionHandler; (*Save system handler here*)
	debughandler:DebugHandler;
TYPE

	BPList* = OBJECT
	VAR
		list*,
		tmp*:BrkPnt;
		active:BOOLEAN;
		PROCEDURE &Init*(*CONST textView : WMTextView.TextView*);
		BEGIN
(*?
Stupid
  *)
(*  			KernelLog.String("brkPnt = active"); KernelLog.Ln; *)
			active:=TRUE; 
		END Init;

		PROCEDURE AddBP*(adr:ADDRESS);
		VAR
			p,next,last:BrkPnt;
		BEGIN
			next:=list;

			NEW(p);
			p.adr:=adr;
			IF next#NIL THEN
				WHILE (next#NIL)&(next.adr<adr) DO last:=next; next:=next.next END;
				IF next=NIL THEN
					last.next:=p;
				ELSE
					IF next.adr=adr THEN RETURN END;
					p.adr:=next.adr;next.adr:=adr;
					IF Trace THEN KernelLog.String("SetBrk="); KernelLog.Hex(adr,0);  KernelLog.Ln; END;
					SetBrk(next);
					p.next:=next.next;
					next.next:=p;
				END;
			ELSE
				list:=p;
				IF Trace THEN KernelLog.String("SetBrk="); KernelLog.Hex(adr,0);  KernelLog.Ln; END;
				SetBrk(p);
			END;
			IF Trace THEN
				DumpList(list);
			END;
		END AddBP;

		PROCEDURE FindBP(lst:BrkPnt;adr:ADDRESS):BOOLEAN;
		VAR
			next:BrkPnt;
		BEGIN
			next:=lst;
			WHILE (next#NIL)&(next.adr#adr) DO next:=next.next END;
			RETURN next#NIL;
		END FindBP;
		
		PROCEDURE AddTmpBP*(adr:ADDRESS);
		VAR
			p:BrkPnt;
			(* cmd:ARRAY 20 OF CHAR; *)
		BEGIN
			KernelLog.String("AddTmpBP "); KernelLog.Address(adr); 
			IF FindBP(list,adr) THEN
				KernelLog.String(" uzhe estq postojannaja"); KernelLog.Ln; RETURN END;
			IF FindBP(tmp,adr) THEN 
				KernelLog.String(" uzhe estq vrem"); KernelLog.Ln; RETURN END; (*no add if now real BP*)
			NEW(p);
			p.adr:=adr;
			p.next:=tmp;tmp:=p;
			SetBrk(p);
		END AddTmpBP;

		PROCEDURE RemoveTmpBP*;
		BEGIN
			WHILE tmp#NIL DO
				ClrBrk(tmp);
				tmp:=tmp.next;
			END;
			tmp:=NIL;
		END RemoveTmpBP;

 
		PROCEDURE RemoveBP*(adr:ADDRESS);
		VAR
			next,last:BrkPnt;
		BEGIN
			next:=list;
			WHILE (next#NIL)&(next.adr#adr) DO last:=next; next:=next.next END;
			IF next=NIL THEN RETURN END;
			ClrBrk(next);
			IF next=list THEN list:=next.next;
			ELSE
				IF next.next=NIL THEN
					last.next:=NIL;
				ELSE
					next.adr:=next.next.adr;
					next.next:=next.next.next;
				END;
			END;
			IF Trace THEN
				DumpList(list);
			END;
		END RemoveBP;


		PROCEDURE DumpList*(lst:BrkPnt);
		BEGIN
			KernelLog.String("dh: ");
			WHILE lst#NIL DO
				 KernelLog.Hex(lst.adr, 8); KernelLog.String(", ");
				 lst:=lst.next;
			END;
			KernelLog.Ln;
		END DumpList;
		
		PROCEDURE SetBrk*(bp:BrkPnt);
		BEGIN	
			KernelLog.String("SetBrk "); KernelLog.Address(bp.adr);
			IF active THEN
				KernelLog.String(" active, ustanovlju"); 
				SYSTEM.GET(bp.adr, bp.c);
				ASSERT(bp.c # 0CCX);
				SYSTEM.PUT(bp.adr, 0CCX) END;
			KernelLog.String("."); KernelLog.Ln;
		END SetBrk;

		PROCEDURE ClrBrk*(bp:BrkPnt);
		BEGIN
				KernelLog.String("clear "); KernelLog.Address(bp.adr); KernelLog.Ln; 
				SYSTEM.PUT(bp.adr, bp.c);
		END ClrBrk;
		
		PROCEDURE ChgState*(state:BOOLEAN);
		VAR
			p:BrkPnt;
		BEGIN
			IF active=state THEN RETURN END;
(*			KernelLog.String("ChgState "); KernelLog.Ln; *)

			active:=state;
			p:=list;
			WHILE p#NIL DO
				IF active THEN
					SetBrk(p);
				ELSE
					ClrBrk(p);
				END;
				p:=p.next;
			END;
			p:=tmp;
			WHILE p#NIL DO
				IF active THEN
					SetBrk(p);
				ELSE
					ClrBrk(p);
				END;
				p:=p.next;
			END;
			IF Trace THEN 
			KernelLog.String("list:");KernelLog.Ln;
			DumpList(list);
			KernelLog.String("tmp:");KernelLog.Ln;
			DumpList(tmp);
			KernelLog.String("...ok!"); KernelLog.Ln; 
			END;
		END ChgState;

	END BPList;
 




PROCEDURE FindEIPByModulePC*(CONST modn:ARRAY OF CHAR;PC:SIGNED32):SIGNED32;
VAR
	m : Modules.Module; 
BEGIN
	m := Modules.root;
	WHILE (m # NIL) & (m.name # modn) DO m := m.next END;
	IF m # NIL THEN
	   RETURN PC+SYSTEM.ADR(m.code[0]);
        ELSE
          RETURN -1;
        END;
END FindEIPByModulePC;

PROCEDURE TestTrap*;
VAR
	LocVar:SIGNED32;
BEGIN
	LocVar:=45H;
	CODE	INT 3	END;
END TestTrap;



	(* Get a compressed refblk number. *)
	PROCEDURE GetNum( refs: Modules.Bytes;  VAR i, num: SIGNED32 );  
	VAR n, s: SIGNED32;  x: CHAR;  
	BEGIN 
		s := 0;  n := 0;  x := refs[i];  INC( i );  
		WHILE ORD( x ) >= 128 DO INC( n, ASH( ORD( x ) - 128, s ) );  INC( s, 7 );  x := refs[i];  INC( i ) END;  
		num := n + ASH( ORD( x ) MOD 64 - ORD( x ) DIV 64 * 64, s )
	END GetNum; 

	PROCEDURE NewObjectFile*(refs: Modules.Bytes): BOOLEAN;
	BEGIN
		RETURN (refs # NIL) & (LEN(refs) >0) & (refs[0]=0FFX);
	END NewObjectFile;

	(* Find a procedure in the reference block.  Return index of name, or -1 if not found. *)
	PROCEDURE FindProc*(refs: Modules.Bytes; modpc: ADDRESS; VAR startpc: ADDRESS): SIGNED32;
	VAR pos, len, t, tstart, tend, proc: SIGNED32; ch: CHAR; newObjectFile, found: BOOLEAN;
	BEGIN
		IF (refs=NIL) OR (LEN(refs) = 0) THEN RETURN -1 END;
		newObjectFile := NewObjectFile(refs);
		proc := -1; pos := 0; len := LEN(refs^);
		IF newObjectFile THEN INC(pos) END;
		ch := refs[pos]; INC(pos); tstart := 0;
		found := FALSE;
		WHILE ~found & (pos < len) & ((ch = 0F8X) OR (ch = 0F9X)) DO	(* proc *)
			GetNum(refs, pos, tstart);	(* procedure offset *)
			IF newObjectFile THEN
				GetNum(refs,pos,tend);
				found := (tstart <=modpc) & (tend  > modpc)
			ELSE
				found := tstart > modpc
			END;
				IF ch = 0F9X THEN
					GetNum(refs, pos, t);	(* nofPars *)
					INC(pos, 3)	(* RetType, procLev, slFlag *);
					IF newObjectFile THEN INC(pos,6) END;
				END;
			IF ~found THEN (* not yet found -- remember startpc and position for next iteration *)
				startpc := tstart;
				proc := pos;	(* remember this position, just before the name *)
				REPEAT ch := refs[pos]; INC(pos) UNTIL ch = 0X;	(* pname *)
				IF pos < len THEN
					ch := refs[pos]; INC(pos);	(* 1X | 3X | 0F8X | 0F9X *)
					WHILE (pos < len) & (ch >= 1X) & (ch <= 3X) DO	(* var *)
						ch := refs[pos]; INC(pos);	(* type *)
						IF (ch >= 81X) OR (ch = 16X) OR (ch = 1DX) THEN
							GetNum(refs, pos, t)	(* dim/tdadr *)
						END;
						GetNum(refs, pos, t);	(* vofs *)
						REPEAT ch := refs[pos]; INC(pos) UNTIL ch = 0X;	(* vname *)
						IF pos < len THEN ch := refs[pos]; INC(pos) END	(* 1X | 3X | 0F8X | 0F9X *)
					END
				END
			END;
		END;
		IF newObjectFile THEN
			IF found THEN
				startpc := tstart; proc := pos;
			ELSE proc := -1
			END;
		END;
		RETURN proc
	END FindProc;

	(* Debbuger exception handler. *)
	PROCEDURE Exception( VAR int: Kernel32.Context;  VAR exc: Kernel32.ExceptionRecord;  VAR handled: BOOLEAN );
	BEGIN  (* interrupts off *)
		handled := FALSE;
		IF Trace THEN KernelLog.String("Debug exception handler! "); KernelLog.Ln; END;
		IF debughandler#NIL THEN
			debughandler(int,exc,handled);
			IF Trace THEN KernelLog.String("user complit! "); KernelLog.Ln; END;
		END;
(*		IF 8 IN int.ContextFlags THEN
			EXCL(int.ContextFlags,8);
		ELSE
			INCL(int.ContextFlags,8);
		END;
*)
		IF (~handled)&(exceptionhandler#NIL) THEN
			IF Trace THEN KernelLog.String("do system! "); KernelLog.Ln; END;
			exceptionhandler(int,exc,handled);
		END;
	FINALLY
		(* if trap occurs in this procedure, then go on working right here *)
(*		KernelLog.String("Houston, we have a problem!"); KernelLog.Ln; *)
	END Exception;

PROCEDURE Install*;
VAR
	adr:ADDRESS;
BEGIN
	PodrobnajaPechatq.GetProcedure('Traps', 'Exception',adr);
	exceptionhandler:=SYSTEM.VAL(Objects.ExceptionHandler,adr);
	Objects.InstallExceptionHandler( Exception );
END Install;

(** *)
PROCEDURE InstallDbgHandler*(hndlr:DebugHandler);
BEGIN
	debughandler:=hndlr;
	Install();
END InstallDbgHandler;

PROCEDURE RemoveDbgHandler*();
BEGIN
	debughandler:=NIL;
END RemoveDbgHandler;

	(** Find global variables of mod (which may be NIL) and return it in the refs, refpos and base parameters for use by NextVar.  If not found, refpos returns -1. *)
	PROCEDURE InitVar*(mod: Modules.Module; VAR refs: Modules.Bytes; VAR refpos: SIGNED32; VAR base: ADDRESS);
	VAR ch: CHAR; startpc: ADDRESS;
	BEGIN
		refpos := -1;
		IF mod # NIL THEN
			refs := mod.refs; base := mod.sb;
			IF (refs # NIL) & (LEN(refs) # 0) THEN
				refpos := FindProc(refs, 0, startpc);
				IF refpos # -1 THEN
					ch := refs[refpos]; INC(refpos);
					WHILE ch # 0X DO ch := refs[refpos]; INC(refpos) END
				END
			END
		END
	END InitVar;

PROCEDURE GetProcedure*(CONST moduleName, procedureName : ARRAY OF CHAR; VAR entryAdr : ADDRESS);
BEGIN PodrobnajaPechatq.GetProcedure(moduleName, procedureName, entryAdr) END GetProcedure; 

PROCEDURE TestProcObj*;
VAR
	adr:ADDRESS;
BEGIN
(*	WMTextView2.TextView.RenderAboveTextMarkers*)
	PodrobnajaPechatq.GetProcedure('BtDTraps', 'SetBrk',adr);
	KernelLog.String("Adr "); KernelLog.Hex(adr, 0); KernelLog.Ln;
	PodrobnajaPechatq.GetProcedure('BtDTraps', '$$',adr);
	KernelLog.String("Adr "); KernelLog.Hex(adr, 0); KernelLog.Ln;
	PodrobnajaPechatq.GetProcedure('BtDTraps', 'Cleanup',adr);
	KernelLog.String("Adr "); KernelLog.Hex(adr, 0); KernelLog.Ln;
END TestProcObj;


(** Termination handler. Removes the window from the display space when the module is unloaded. *)
PROCEDURE Cleanup;	(* K *)
BEGIN
		IF exceptionhandler#NIL THEN
			Objects.InstallExceptionHandler( exceptionhandler );
			KernelLog.String( "Traps: TrapHandler Restored!" );  KernelLog.Ln;
		ELSE
			KernelLog.String( "Not need to restore traps handler !" );  KernelLog.Ln;
		END;
END Cleanup;

BEGIN
	Modules.InstallTermHandler(Cleanup);
	NEW(brkPnt);

END BtDTraps.


BtDTraps.TestTrap~

BtDTraps.Install~

BtDTraps.TestProcObj~

System.Free  BtDTraps ~