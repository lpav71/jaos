(* UCS2 ~= UTF16 *)
MODULE UCS2; 

IMPORT UCS32;

(* Nazvanie UCS2 podcherkivaet, chto polnaja podderzhka surrogantnykh par mozhet otsutstvovatq. 
   http://www.unicode.org/faq/utf_bom.html#utf16-11
   *) 
   
(* Ehtot modulq dolzhen gruzitqsja rano po techeniju sborki i ne bytq svyazan s podobnymi moduljami dlja drugikh tipov *)
	
CONST UTF16SurrogateOffset* = LSH( UNSIGNED32( 0xD800 ), 10 ) + 0xDC00 - 0x10000;
	KodEot* = 0;
	
TYPE 
	Char* = RECORD UCS2CharCode- : UNSIGNED16 END;
	String* = ARRAY OF Char;
	PString* = POINTER TO String;

PROCEDURE IntToUCS2Char*(iCode : UNSIGNED16) : Char;
 	VAR rez : Char;
	BEGIN	rez.UCS2CharCode := iCode; RETURN rez END IntToUCS2Char;
	
VAR ReplacementChar*, ZeroChar* : Char;

PROCEDURE - UTF16IsSingle( c: Char ): BOOLEAN;
BEGIN
	RETURN SET32( c.UCS2CharCode ) * SET32( 0xFFFFF800 ) # SET32( 0xD800 );
END UTF16IsSingle;

PROCEDURE - UTF16IsSurrogateLead( c: Char ): BOOLEAN;
BEGIN
	RETURN SET32( c.UCS2CharCode ) * SET32( 0x400 ) = { };
END UTF16IsSurrogateLead;

PROCEDURE - UTF16IsLead( c: Char ): BOOLEAN; (* D800–DBFF *)
BEGIN
	RETURN SET32( c.UCS2CharCode ) * SET32( 0xFFFFFC00 ) = SET32( 0xD800 );
END UTF16IsLead;

PROCEDURE - UTF16IsTrail( c: Char ): BOOLEAN; (* DC00–DFFF *)
BEGIN
	RETURN SET32( c.UCS2CharCode ) * SET32( 0xFFFFFC00 ) = SET32( 0xDC00 );
END UTF16IsTrail;

PROCEDURE - UTF16Lead*( supplementary: UNSIGNED32 ): UNSIGNED16;
BEGIN
	RETURN UNSIGNED16( LSH( supplementary, -10 ) + 0xD7C0 );
END UTF16Lead;

PROCEDURE - UTF16Trail*( supplementary: UNSIGNED32 ): UNSIGNED16;
BEGIN
	RETURN UNSIGNED16( SET32( supplementary ) * SET32( 0x3FF ) + SET32( 0xDC00 ));
END UTF16Trail; 
	
(* JQvNovU2 заменяет слишком большие коды на символ-заместитель *)
PROCEDURE JQvNovU2*( CONST vkh : UCS32.StringJQ) : PString;
VAR b : UCS2StringBuilder; i : SIZE; kod : UNSIGNED32;
BEGIN
	NEW(b, MAX(1,LEN(vkh)));
	i := 0;
	LOOP
		IF i = LEN(vkh) - 1 THEN
			b.AddChar(0); EXIT END;
		kod := vkh[i].UCS32CharCode;
		IF kod >= MAX(UNSIGNED16) THEN
			b.AddChar(ORD('?'));
		ELSIF kod = UCS32.KodEot THEN
			b.AddChar(UCS32.KodEot); EXIT
		ELSE
			b.AddChar(UNSIGNED16(kod)) END;
		INC(i) END;
	RETURN b.GetString() END JQvNovU2;
	
	
(* Обязательно завершаем нулём. Если места не хватит, придётся обрезать! *)
PROCEDURE U8вU2ОбрезаяИДобиваяНулём*(u8: ARRAY OF CHAR; VAR u2: String); 
VAR промеж: UCS32.PStringJQ; i : SIZE; kod : UNSIGNED32;
BEGIN
	промеж := UCS32.U8vNovJQ(u8);
	i := 0;
	ASSERT(LEN(u2)>0);
	LOOP
		IF (i = LEN(u2) - 1) OR (i = LEN(u8) - 1) THEN
			u2[i].UCS2CharCode := UCS32.KodEot; EXIT END;
		kod := промеж[i].UCS32CharCode;
		IF kod >= MAX(UNSIGNED16) THEN
			u2[i].UCS2CharCode := ORD('?');
		ELSIF kod = UCS32.KodEot THEN
			u2[i].UCS2CharCode := UCS32.KodEot; EXIT
		ELSE
			u2[i].UCS2CharCode := UNSIGNED16(kod) END;
		INC(i) END;
	RETURN END U8вU2ОбрезаяИДобиваяНулём;
	
PROCEDURE U2вU8ОбрезаяИДобиваяНулём*(u2: String; VAR u8: ARRAY OF CHAR);
BEGIN COPY(U2vNovU8(u2)^,u8) END U2вU8ОбрезаяИДобиваяНулём;

(* см. примечание у U8vNovU2 *)	
PROCEDURE U2vNovU8*(CONST u2 : String) : POINTER TO ARRAY OF CHAR;
VAR промеж: UCS32.PStringJQ;
BEGIN
	промеж := U2vNovJQ(u2);
	RETURN UCS32.JQvNovU8(промеж^) END U2vNovU8;

(* U8vNovU2 копирует строку в UTF8 на кучу в UCS2. Безобразие в том, что
  это делается через промежуточную StringJQ *)
PROCEDURE U8vNovU2*(u8 : ARRAY OF CHAR) : PString;
VAR промеж: UCS32.PStringJQ;
BEGIN
	промеж := UCS32.U8vNovJQ(u8);
	RETURN JQvNovU2(промеж^) END U8vNovU2;

	
PROCEDURE U2vNovJQ*( CONST clipA2UCS2String: String ): UCS32.PStringJQ;
VAR
	rez: UCS32.PStringJQ;
	clipboardPos := 0, textPos := 0, clipboardLen := 0: SIZE;
	codePoint : UNSIGNED32; lead, trail: Char;
BEGIN
	clipboardLen := LEN(clipA2UCS2String);
	NEW(rez, clipboardLen + 1);
	WHILE clipboardPos < clipboardLen DO
		codePoint := ReplacementChar.UCS2CharCode;
		lead := clipA2UCS2String[ clipboardPos ];

		(* make unicode *)
		IF UTF16IsSingle( lead ) THEN
			codePoint := lead.UCS2CharCode;
			INC( clipboardPos );
		ELSE
			IF UTF16IsSurrogateLead( lead ) THEN
				INC( clipboardPos );
				IF ( clipboardPos < clipboardLen ) THEN
					trail := clipA2UCS2String[ clipboardPos ];
					IF UTF16IsTrail( trail ) THEN
						codePoint := LSH( lead.UCS2CharCode, 10 ) + trail.UCS2CharCode - UTF16SurrogateOffset;
						INC( clipboardPos );
					END;
				END;
			ELSIF clipboardPos > 0 THEN
				trail := clipA2UCS2String[ clipboardPos - 1 ];
				IF UTF16IsLead( trail ) THEN
					codePoint := LSH( trail.UCS2CharCode, 10 ) + lead.UCS2CharCode - UTF16SurrogateOffset;
				END;
				INC( clipboardPos );
			END;
		END;

		(* CRLF -> LF *)
		IF ( codePoint = UCS32.KodCR ) THEN
			codePoint := UCS32.KodLF;
			IF ( clipboardPos < clipboardLen ) & ( clipA2UCS2String[ clipboardPos ].UCS2CharCode = UCS32.KodLF ) THEN
				INC( clipboardPos );
			END;
		END;

		(* accept codePoint *)
		rez[ textPos ] := UCS32.IntToCharJQ( codePoint );
		INC( textPos );
	END;

	IF textPos # 0 THEN
		rez[ textPos ] := UCS32.IntToCharJQ(UCS32.KodEot);
	END;
	RETURN rez
END U2vNovJQ;


TYPE
	UCS2StringBuilder* = OBJECT
	VAR
		length: SIZE;
		data: PString;

		PROCEDURE &Init*( initialSize: SIZE );
		BEGIN
			IF initialSize < 260 THEN initialSize := 260 END;
			NEW( data, initialSize );
			data[ 0 ] := ZeroChar; 
			length := 0;
		END Init;

		PROCEDURE Clear*;
		BEGIN
			data[ 0 ] := ZeroChar;
			length := 0;
		END Clear;

		PROCEDURE AddChar*( codePoint: UNSIGNED16 );
		VAR i, newLength: SIZE; newData: PString;
		BEGIN
			IF length + 2 >= LEN( data ) THEN
				newLength := MIN(MAX(SIZE) - 2, length + MAX( 16, length DIV 2 ));
				NEW( newData, newLength );
				FOR i := 0 TO length - 1 DO newData[ i ] := data[ i ]; END;
				data := newData;
			END;
			data[ length ] := IntToUCS2Char(codePoint);
			INC( length );
			data[ length ] := ZeroChar;
		END AddChar;

		PROCEDURE GetString*( ): PString;
		VAR rez : PString; i : SIZE;
		BEGIN
			NEW(rez, length);
			FOR i := 0 TO length - 1 DO rez[i] := data[i] END;
			RETURN data
		END GetString;

	END UCS2StringBuilder;
	

BEGIN
ReplacementChar := IntToUCS2Char( 0xFFFD );
END UCS2.

