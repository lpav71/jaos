MODULE LisTextCompiler; (** AUTHOR ""; PURPOSE ""; *)

IMPORT Streams, Modules, Compiler, LisCompiler, TextUtilities, Diagnostics, Texts, 
	LisSyntaxTree, 
	CompilerInterface, 
	UTF8Strings, Commands, 
	LisRasshirenieOpcijjKompiljatora;

CONST

	LisName = "Lis";
	LisDescription = "Jar Compiler";
	LisFileExtension = "ярм"; (*! temporary *)


	PROCEDURE GetClipboardReader(): Streams.Reader;
	VAR size : SIZE;
		selectionText: Texts.Text;
		pcStr: POINTER TO ARRAY OF CHAR;
		stringReader: Streams.StringReader;
	BEGIN
		selectionText := Texts.clipboard;
		selectionText.AcquireRead;
		size := UTF8Size(selectionText,0,selectionText.GetLength())+1;
		NEW(pcStr,size);
		TextUtilities.SubTextToStr(selectionText,0,size,pcStr^);
		selectionText.ReleaseRead;
		NEW(stringReader,size);
		stringReader.Set(pcStr^);
		RETURN stringReader;
	END GetClipboardReader;

	PROCEDURE GetSelectionReader(): Streams.Reader;
	VAR a, b, size : SIZE;
		selectionText: Texts.Text;
		from, to: Texts.TextPosition;
		pcStr: POINTER TO ARRAY OF CHAR;
		stringReader: Streams.StringReader;
	BEGIN
		IF Texts.GetLastSelection(selectionText, from, to) THEN
			selectionText.AcquireRead;
			a := MIN(from.GetPosition(), to.GetPosition());
			b := MAX(from.GetPosition(), to.GetPosition());
			size := UTF8Size(selectionText,a,b-a+1)+1;
			NEW(pcStr,size);
			TextUtilities.SubTextToStr(selectionText, a, b - a+1, pcStr^);
			selectionText.ReleaseRead;
			NEW(stringReader,b-a+1);
			stringReader.Set(pcStr^);
		ELSE
			stringReader  := NIL;
		END;
		RETURN stringReader;
	END GetSelectionReader;

	PROCEDURE GetTextReader(text: Texts.Text; position: SIZE): Streams.Reader;
	VAR size : SIZE;
		pcStr: POINTER TO ARRAY OF CHAR;
		stringReader: Streams.StringReader;
	BEGIN
		text.AcquireRead;
		size := UTF8Size(text,position,text.GetLength())+1;
		NEW(pcStr,size);
		TextUtilities.SubTextToStr(text,position,size,pcStr^);
		text.ReleaseRead;
		NEW(stringReader,size);
		stringReader.Set(pcStr^);
		RETURN stringReader;
	END GetTextReader;

	PROCEDURE UTF8Size(text : Texts.Text; startPos, len : SIZE): SIZE;
	VAR i, length, pos,size : SIZE; r : Texts.TextReader; ch : Texts.Char32; ok : BOOLEAN;
		string: ARRAY 16 OF CHAR;
	BEGIN
		text.AcquireRead;
		NEW(r, text);
		r.SetPosition(startPos);
		size := 0;i := 0; length := len; ok := TRUE;
		WHILE (i < length) & ok DO
			r.ReadCh(ch);
			IF (ch > 0) THEN
				pos := 0;
				ok := UTF8Strings.EncodeChar(ch, string, pos);
				INC(size,pos);
			END;
			INC(i);
		END;
		text.ReleaseRead;
		RETURN size
	END UTF8Size;

	PROCEDURE LisCompileText*(t: Texts.Text; CONST source: ARRAY OF CHAR; pos: SIGNED32; CONST pc,opt: ARRAY OF CHAR; log: Streams.Writer;
	diagnostics : Diagnostics.Diagnostics; VAR error: BOOLEAN);
	VAR stringReader: Streams.StringReader;
		reader: Streams.Reader;
		options: LisCompiler.CompilerOptions;
		importCache: LisSyntaxTree.ModuleScope;
	BEGIN
		IF t = NIL THEN
			log.String ("No text available"); log.Ln; log.Update;
			error := TRUE; RETURN
		END;
		NEW(stringReader,LEN(opt));
		stringReader.Set(opt);
		IF LisCompiler.GetOptions(stringReader,log,diagnostics,options) THEN
			WITH diagnostics :
				| LisRasshirenieOpcijjKompiljatora.ExtractCompilerOptionsDiagnostics DO
					diagnostics.compilerOptions := options;
					RETURN; 
				ELSE END;
			(* raskommentirujj ehto, chtoby poluchatq stek na kazhduju oshibku kompiljacii:
			diagnostics := Basic.GetTracingDiagnostics(diagnostics); *)
			reader := GetTextReader(t,pos);
			IF pc # "" THEN INCL(options.flags,Compiler.FindPC); COPY(pc, options.findPC);
			ELSIF options.findPC # "" THEN INCL(options.flags,LisCompiler.FindPC)
			END;
			error := ~LisCompiler.Modules(source,reader,0,diagnostics, log, options, importCache)
		END;
	END LisCompileText;



	PROCEDURE CompileSelection*(context: Commands.Context);
	BEGIN
		Compiler.CompileReader(context,GetSelectionReader())
	END CompileSelection;

	PROCEDURE CompileClipboard*(context: Commands.Context);
	BEGIN
		Compiler.CompileReader(context,GetClipboardReader())
	END CompileClipboard;

PROCEDURE Cleanup;
BEGIN
	CompilerInterface.Unregister(LisName);
END Cleanup;

BEGIN
	CompilerInterface.Register(LisName, LisDescription, LisFileExtension, LisCompileText);
	Modules.InstallTermHandler(Cleanup);
END LisTextCompiler.
